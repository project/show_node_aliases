INTRODUCTION
------------

Show Node Aliases module enables privileged users to see, edit and delete
all url aliases (paths) assigned to a node, from within the node edit page.


REQUIREMENTS
------------

This module requires the following modules to be enabled:
 * Path (https://www.drupal.org/documentation/modules/path)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, a table will be added to a node path form with a list of all aliases
assigned to that node.


MAINTAINERS
-----------

Current maintainers:
 * Artem Kolotilkin - https://drupal.org/user/1915462
